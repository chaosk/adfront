from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms


class CampaignForm(forms.Form):
	# @TODO fieldset/formset pls
	advertiserId = forms.IntegerField(widget=forms.HiddenInput())
	campaignName = forms.CharField(label="Campaign name", max_length=255)
	startDate = forms.DateField(label="Start date", widget=forms.DateInput(), required=False)
	endDate = forms.DateField(label="End date", widget=forms.DateInput(), required=False)
	impressions = forms.IntegerField(label="Number of booked impressions", required=False)
	clicks = forms.IntegerField(label="Number of booked clicks", required=False)

	revenue = forms.FloatField(required=False)
	REVENUE_TYPE_CPM = 1
	REVENUE_TYPE_CPC = 2
	REVENUE_TYPE_CPA = 3
	REVENUE_TYPE_MT = 4
	REVENUE_TYPES = (
		(REVENUE_TYPE_CPM, "CPM"),
		(REVENUE_TYPE_CPC, "CPC"),
		(REVENUE_TYPE_CPA, "CPA"),
		(REVENUE_TYPE_MT, "Monthly Tennancy"),
	)
	revenueType = forms.TypedChoiceField(label="Pricing model", required=False,
		choices=REVENUE_TYPES, coerce=int)

	priority = forms.IntegerField(label="Priority level", required=False)
	weight = forms.IntegerField(label="Campaign weight", required=False)

	# frequency capping
	capping = forms.IntegerField(label="Total views per user", required=False)
	sessionCapping = forms.IntegerField(label="Total views per period", required=False)
	block = forms.IntegerField(label="Period used in the session capping", required=False)

	def __init__(self, *args, **kwargs):
		super(CampaignForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_id = 'form'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))

"""
'advertiserId', 'campaignName', 'startDate', 'endDate',
                         'impressions', 'clicks', 'priority', 'weight',
                         'targetImpressions', 'targetClicks', 'targetConversions',
                         'revenue', 'revenueType',
                         'capping', 'sessionCapping', 'block', 'comments',
                         'viewWindow', 'clickWindow'
"""


class ZoneForm(forms.Form):
	publisherId = forms.IntegerField(widget=forms.HiddenInput())
	type = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
	zoneName = forms.CharField(label="Zone name", max_length="255")
	width = forms.IntegerField(label="Width")
	height = forms.IntegerField(label="Height")

	def __init__(self, *args, **kwargs):
		super(ZoneForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_id = 'form'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))


class GenericDeleteForm(forms.Form):
	pass
