from django.conf.urls import patterns, include, url
from deathtrap.views import CampaignAdd, CampaignEdit, CampaignDelete, CampaignDetail, CampaignList
from deathtrap.views import BannerAdd, BannerEdit, BannerDetail, BannerZones, BannerZoneLink, BannerList
from deathtrap.views import ZoneAdd, ZoneEdit, ZoneDetail, ZoneDelete, ZoneList

urlpatterns = [
	url(r'campaign/add/$', CampaignAdd.as_view(), name="campaign-add"),
	url(r'campaign/edit/(?P<campaign_id>\d+)/$', CampaignEdit.as_view(), name="campaign-edit"),
	url(r'campaign/detail/(?P<campaign_id>\d+)/$', CampaignDetail.as_view(), name="campaign-detail"),
	url(r'campaign/delete/(?P<campaign_id>\d+)/$', CampaignDelete.as_view(), name="campaign-delete"),
	url(r'campaign/list/$', CampaignList.as_view(), name="campaign-list"),
	url(r'campaign/list/(?P<advertiser_id>\d+)/$', CampaignList.as_view(), name="campaign-list"),

	url(r'banner/add/(?P<campaign_id>\d+)/$', BannerAdd.as_view(), name="banner-add"),
	url(r'banner/edit/(?P<banner_id>\d+)/$', BannerEdit.as_view(), name="banner-edit"),
	url(r'banner/detail/(?P<banner_id>\d+)/$', BannerDetail.as_view(), name="banner-detail"),
	url(r'banner/zones/(?P<banner_id>\d+)/$', BannerZones.as_view(), name="banner-zones"),
	url(r'banner/link/(?P<banner_id>\d+)/(?P<zone_id>\d+)/$', BannerZoneLink.as_view(), name="banner-zone-link"),
	url(r'banner/list/(?P<campaign_id>\d+)/$', BannerList.as_view(), name="banner-list"),

	url(r'zone/add/$', ZoneAdd.as_view(), name="zone-add"),
	url(r'zone/edit/(?P<zone_id>\d+)/$', ZoneEdit.as_view(), name="zone-edit"),
	url(r'zone/detail/(?P<zone_id>\d+)/$', ZoneDetail.as_view(), name="zone-detail"),
	url(r'zone/delete/(?P<zone_id>\d+)/$', ZoneDelete.as_view(), name="zone-delete"),
	url(r'zone/list/$', ZoneList.as_view(), name="zone-list"),
]
