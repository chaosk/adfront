from contextlib import contextmanager
from functools import partial
try:
	import xmlrpc.client as xmlrpcclient
except ImportError:
	import xmlrpclib as xmlrpcclient

METHODS = ['ox.addAdvertiser','ox.addAgency','ox.addBanner','ox.addCampaign',
	'ox.addChannel','ox.addPublisher','ox.addTracker','ox.addUser','ox.addVariable',
	'ox.addZone','ox.advertiserBannerStatistics','ox.advertiserCampaignStatistics',
	'ox.advertiserDailyStatistics','ox.advertiserPublisherStatistics',
	'ox.advertiserZoneStatistics','ox.agencyAdvertiserStatistics',
	'ox.agencyBannerStatistics','ox.agencyCampaignStatistics',
	'ox.agencyDailyStatistics','ox.agencyPublisherStatistics','ox.agencyZoneStatistics',
	'ox.bannerDailyStatistics','ox.bannerPublisherStatistics','ox.bannerZoneStatistics',
	'ox.campaignBannerStatistics','ox.campaignConversionStatistics',
	'ox.campaignDailyStatistics','ox.campaignPublisherStatistics',
	'ox.campaignZoneStatistics','ox.deleteAdvertiser','ox.deleteAgency',
	'ox.deleteBanner','ox.deleteCampaign','ox.deleteChannel','ox.deletePublisher',
	'ox.deleteTracker','ox.deleteUser','ox.deleteVariable','ox.deleteZone',
	'ox.generateTags','ox.getAdvertiser','ox.getAdvertiserListByAgencyId',
	'ox.getAgency','ox.getAgencyList','ox.getBanner','ox.getBannerListByCampaignId',
	'ox.getBannerTargeting','ox.getCampaign','ox.getCampaignListByAdvertiserId',
	'ox.getChannel','ox.getChannelListByAgencyId','ox.getChannelListByWebsiteId',
	'ox.getChannelTargeting','ox.getPublisher','ox.getPublisherListByAgencyId',
	'ox.getTracker','ox.getUser','ox.getUserList','ox.getUserListByAccountId',
	'ox.getVariable','ox.getZone','ox.getZoneListByPublisherId','ox.linkBanner',
	'ox.linkCampaign','ox.linkTrackerToCampaign','ox.linkUserToAdvertiserAccount',
	'ox.linkUserToManagerAccount','ox.linkUserToTraffickerAccount','ox.logoff',
	'ox.logon','ox.modifyAdvertiser','ox.modifyAgency','ox.modifyBanner',
	'ox.modifyCampaign','ox.modifyChannel','ox.modifyPublisher','ox.modifyTracker',
	'ox.modifyUser','ox.modifyVariable','ox.modifyZone',
	'ox.publisherAdvertiserStatistics','ox.publisherBannerStatistics',
	'ox.publisherCampaignStatistics','ox.publisherDailyStatistics',
	'ox.publisherZoneStatistics','ox.setBannerTargeting','ox.setChannelTargeting',
	'ox.unlinkBanner','ox.unlinkCampaign','ox.updateSsoUserId',
	'ox.updateUserEmailBySsoId','ox.zoneAdvertiserStatistics','ox.zoneBannerStatistics',
	'ox.zoneDailyStatistics','ox.zoneCampaignStatistics','system.listMethods',
	'system.methodHelp','system.methodSignature']


class ReviveXMLRPCClient(object):
	"""
	Usage:
		client = ReviveXMLRPCClient(endpoint)
		with client.session(username, password):
			client.getUser(1)

	Alternatively, reusing a single sessionid might be prefered:
		sessionid = cache.get('sessionid')
		client = ReviveXMLRPCClient(endpoint, sessionid)
		if not sessionid:
			sessionid = client.logon(username, password)
			cache.set('sessionid', sessionid, 24*60*60)
		client.getUser(1)
	"""

	METHOD_PREFIX = 'ox'
	PROTECTED_METHODS = ['logon', 'logoff']

	def __init__(self, endpoint, sessionid=None):
		super(ReviveXMLRPCClient, self).__init__()
		self._sessionid = sessionid
		self.proxy = xmlrpcclient.ServerProxy(endpoint)
		if sessionid:
			self.construct_methods()

	@contextmanager
	def session(self, user, password):
		self.logon(user, password)
		yield
		self.logoff()

	def construct_methods(self):
		"""
		Retrieves available methods,
		adds them to self as partials
		with first argument being sessionid.
		"""
		for prefix, method in (m.split('.') for m in METHODS):
			if getattr(self, method, None) or prefix != self.METHOD_PREFIX:
				# won't overwrite existing methods
				# won't implement system methods
				continue
			setattr(self, method, partial(
				getattr(getattr(self.proxy, self.METHOD_PREFIX), method),
				self._sessionid))

	def destruct_methods(self):
		for prefix, method in (m.split('.') for m in METHODS):
			if method in self.PROTECTED_METHODS or prefix != self.METHOD_PREFIX:
				# won't delete pre-existing methods
				continue
			delattr(self, method)

	def logon(self, user, password):
		if self._sessionid:
			self.destruct_methods()
		self._sessionid = self.proxy.ox.logon(user, password)
		if self._sessionid:
			self.construct_methods()
		return self._sessionid

	def logoff(self):
		self.proxy.ox.logoff(self._sessionid)
		self.destruct_methods()

