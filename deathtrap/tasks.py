from __future__ import print_function
from celery.task import task
from django.core.cache import cache
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from deathtrap.reviveclient import ReviveXMLRPCClient

REVIVE_SETTINGS_REQUIRED_KEYS = ("ENDPOINT", "USER", "PASSWORD")

@task
def xmlrpc_call(method, params=None):
	"""
	Sends an API call to the Revive adserver.
	"""
	# @TODO cache invalidation
	if params is None:
		params = []
	if method.startswith('get'):
		cached = cache.get('xmlrpc_{}_{}'.format(hash(method), hash(tuple(params))))
		if cached:
			return cached
	revive_settings = settings.REVIVE_CLIENT
	if revive_settings is None or any(key not in revive_settings for key in REVIVE_SETTINGS_REQUIRED_KEYS):
		raise ImproperlyConfigured(
			"The REVIVE_CLIENT setting must be set and contain {} keys.".format(
				",".join(REVIVE_SETTINGS_REQUIRED_KEYS)))
	sessionid = cache.get('sessionid', None)
	client = ReviveXMLRPCClient(revive_settings["ENDPOINT"], sessionid)
	if not sessionid:
		sessionid = client.logon(revive_settings["USER"], revive_settings["PASSWORD"])
		cache.set('sessionid', sessionid, 5*60)
	print("Calling {}({})".format(method, ",".join(str(param) for param in params)))
	response = getattr(client, method)(*params)
	if method.startswith('get'):
		cache.set('xmlrpc_{}_{}'.format(hash(method), hash(tuple(params))),
			response, 5*60)
	return response
