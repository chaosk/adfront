from django.db import models

class Banner(models.Model):
	banner_id = models.IntegerField()
	campaign_id = models.IntegerField()
	name = models.CharField(max_length=255, blank=True)
	url = models.CharField(max_length=255, blank=True)
	layers_file = models.FileField(upload_to="banner/meta")
	result_file = models.ImageField(upload_to="banner/result")
