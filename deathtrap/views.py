import base64
import json
import uuid
from django.core.exceptions import ImproperlyConfigured
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext
from django.utils.encoding import force_text
from django.views.generic import CreateView, TemplateView, ListView
from crispy_forms.utils import render_crispy_form
from deathtrap.banner import Painter
from deathtrap.forms import CampaignForm, ZoneForm, GenericDeleteForm
from deathtrap.models import Banner
from deathtrap.tasks import xmlrpc_call


class XMLRPCReadMethodMixin(object):
	method = None
	template_name = 'deathtrap/base/detail.jinja'

	def get(self, request, *args, **kwargs):
		context = self.get_context_data(**kwargs)
		if request.is_ajax():
			return HttpResponse(json.dumps(context))
		return self.render_to_response(context)

	def get_context_data(self, **kwargs):
		context = super(XMLRPCReadMethodMixin, self).get_context_data(**kwargs)
		context["tasks"] = []
		if not getattr(self, 'tasks', None):
			self.tasks = ((self.method, self.shared_template),)
		for task_data in self.tasks:
			method, template = task_data
			task = xmlrpc_call.delay(method, self.get_rpc_args(**kwargs))
			task_output = {"task_id": task.task_id, "template": template}
			context["tasks"].append(task_output)
		return context

	def get_rpc_args(self, **kwargs):
		return []


class XMLRPCWriteMethodMixin(object):
	get_method = None
	post_method = None
	template_name = 'deathtrap/base/form.jinja'

	def post(self, request, *args, **kwargs):
		request.POST = request.POST.copy()
		request.POST.update(self.form_args(**kwargs))
		form = self.form_class(request.POST)
		context = {'success_url': self.get_success_url()}
		if form.is_valid():
			if request.is_ajax():
				task = xmlrpc_call.delay(self.post_method, self.post_rpc_args(form, **kwargs))
				context.update({'success': True, 'task_id': task.task_id})
				return HttpResponse(json.dumps(context))
			else:
				result = xmlrpc_call(self.post_method, self.post_rpc_args(form, **kwargs))
				return HttpResponseRedirect(reverse(self.get_success_url(), result))
		rendered_form = render_crispy_form(form, context=RequestContext(request))
		if request.is_ajax():
			context.update({'success': False, 'form_html': rendered_form})
			return HttpResponse(json.dumps(context))
		context.update({'form': rendered_form})
		return self.render_to_response(context)

	def get_context_data(self, **kwargs):
		context = super(XMLRPCWriteMethodMixin, self).get_context_data(**kwargs)
		context["success_url"] = self.get_success_url()
		context["form"] = render_crispy_form(self.form_class(initial=self.form_args(**kwargs)),
			context=RequestContext(self.request))
		if getattr(self, 'shared_template', None):
			context["shared_template"] = self.shared_template
		return context

	def get_success_url(self):
		"""
		Returns the supplied success URL.
		"""
		if self.success_url:
			# Forcing possible reverse_lazy evaluation
			url = force_text(self.success_url)
		else:
			raise ImproperlyConfigured(
				"No URL to redirect to. Provide a success_url.")
		return url

	def get_rpc_args(self, **kwargs):
		return []

	def post_rpc_args(self, form, **kwargs):
		return [{k:v for k,v in form.cleaned_data.iteritems() if v}]

	def form_args(self, **kwargs):
		if self.get_method:
			return xmlrpc_call(self.get_method, self.get_rpc_args(**kwargs))
		return {}


class XMLRPCDeleteMethodMixin(object):
	method = None
	form_class = GenericDeleteForm

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		context = {'success_url': self.get_success_url()}
		if form.is_valid():
			task = xmlrpc_call.delay(self.method, self.get_rpc_args(**kwargs))
			context['task_id'] = task.task_id
			if request.is_ajax():
				return HttpResponse(json.dumps(context))
			else:
				return self.render_to_response(context)
		return self.render_to_response()

	def get_context_data(self, **kwargs):
		context = super(XMLRPCDeleteMethodMixin, self).get_context_data(**kwargs)
		context["success_url"] = self.get_success_url()
		context["form"] = self.form_class()
		return context

	def get_success_url(self):
		"""
		Returns the supplied success URL.
		"""
		if self.success_url:
			# Forcing possible reverse_lazy evaluation
			url = force_text(self.success_url)
		else:
			raise ImproperlyConfigured(
				"No URL to redirect to. Provide a success_url.")
		return url

	def get_rpc_args(self, **kwargs):
		return []


class CampaignAdd(XMLRPCWriteMethodMixin, TemplateView):
	post_method = 'addCampaign'
	success_url = 'campaign-detail'
	form_class = CampaignForm

	def form_args(self, **kwargs):
		return {"advertiserId": self.request.user.revive_advertiser_id}


class CampaignDetail(XMLRPCReadMethodMixin, TemplateView):
	method = 'getCampaign'
	shared_template = 'campaign_detail.jinja'

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('campaign_id'))]


class CampaignEdit(XMLRPCWriteMethodMixin, TemplateView):
	post_method = 'modifyCampaign'
	get_method = 'getCampaign'
	success_url = 'campaign-detail'
	form_class = CampaignForm

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('campaign_id'))]

	def post_rpc_args(self, **kwargs):
		return [int(kwargs.get('campaign_id')), {k:v for k,v in form.cleaned_data.iteritems() if v}]


class CampaignList(XMLRPCReadMethodMixin, TemplateView):
	method = 'getCampaignListByAdvertiserId'
	template_name = 'deathtrap/base/list.jinja'
	shared_template = 'campaign_list.jinja'

	def get_rpc_args(self, **kwargs):
		advertiser_id = kwargs.pop('advertiser_id', None)
		return [advertiser_id if advertiser_id else self.request.user.revive_advertiser_id]


class CampaignDelete(XMLRPCDeleteMethodMixin, TemplateView):
	method = 'deleteCampaign'
	template_name = 'deathtrap/base/delete.jinja'
	success_url = 'campaign-list'

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('campaign_id'))]


class ZoneAdd(XMLRPCWriteMethodMixin, TemplateView):
	post_method = 'addZone'
	success_url = 'zone-detail'
	form_class = ZoneForm

	def form_args(self, **kwargs):
		return {"publisherId": 1}


class ZoneDetail(XMLRPCReadMethodMixin, TemplateView):
	method = 'getZone'
	shared_template = 'zone_detail.jinja'

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('zone_id'))]


class ZoneEdit(XMLRPCWriteMethodMixin, TemplateView):
	get_method = 'getZone'
	post_method = 'modifyZone'
	success_url = 'zone-detail'
	form_class = ZoneForm

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('zone_id'))]

	def post_rpc_args(self, **kwargs):
		return [int(kwargs.get('zone_id')), {k:v for k,v in form.cleaned_data.iteritems() if v}]


class ZoneList(XMLRPCReadMethodMixin, TemplateView):
	method = 'getZoneListByPublisherId'
	template_name = 'deathtrap/base/list.jinja'
	shared_template = 'zone_list.jinja'

	def get_rpc_args(self, **kwargs):
		return [1]


class ZoneDelete(XMLRPCDeleteMethodMixin, TemplateView):
	method = 'deleteZone'
	template_name = 'deathtrap/base/delete.jinja'
	success_url = 'zone-list'

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('zone_id'))]


class BannerAdd(TemplateView):
	template_name = 'deathtrap/banner_add.jinja'

	def post(self, request, *args, **kwargs):
		data = json.loads(request.POST.get('data'))
		painter = Painter((data['canvas']['width'], data['canvas']['height']), data['layers'])
		if data.get('preview'):
			context = {'success': True, 'encoded_image': base64.b64encode(painter.draw())}
			return HttpResponse(json.dumps(context))
		else:
			banner = Banner()
			file_id = uuid.uuid4()
			banner.result_file.save('{}.png'.format(file_id), ContentFile(painter.draw()), save=False)
			banner.layers_file.save('{}.json'.format(file_id), ContentFile(request.POST.get('data')), save=False)
			banner.campaign_id = int(kwargs.get('campaign_id'))
			banner.url = data['url']
			banner.name = data['name']
			banner.banner_id = xmlrpc_call('addBanner', [{
				'campaignId': int(kwargs.get('campaign_id')),
				'bannerName': data['name'],
				'storageType': 'url',
				'imageURL': banner.result_file.url, # fix it with site prefix
				'height': banner.result_file.height,
				'width': banner.result_file.width,
				'url': data['url'],
			}])
			banner.save()
			if request.is_ajax():
				return HttpResponse(json.dumps({'success': True, 'banner_id': banner.banner_id}))
			else:
				return HttpResponseRedirect(reverse('banner-detail', banner_id=banner.banner_id))


class BannerEdit(TemplateView):
	template_name = 'deathtrap/banner_add.jinja'

	def get_context_data(self, **kwargs):
		banner = Banner.objects.get(banner_id=kwargs.get('banner_id'))
		context = super(BannerEdit, self).get_context_data(**kwargs)
		context['banner'] = banner
		return context

	def post(self, request, *args, **kwargs):
		data = json.loads(request.POST.get('data'))
		painter = Painter((data['canvas']['width'], data['canvas']['height']), data['layers'])
		if data.get('preview'):
			context = {'success': True, 'encoded_image': base64.b64encode(painter.draw())}
			return HttpResponse(json.dumps(context))
		else:
			banner = Banner.objects.get(banner_id=kwargs.get('banner_id'))
			file_id = uuid.uuid4()
			banner.result_file.save('{}.png'.format(file_id), ContentFile(painter.draw()))
			banner.layers_file.save('{}.json'.format(file_id), ContentFile(request.POST.get('data')))
			banner.url = data['url']
			banner.name = data['name']
			response = xmlrpc_call('modifyBanner', [{
				'bannerId': banner.banner_id,
				'campaignId': banner.campaign_id,
				'bannerName': data['name'],
				'storageType': 'url',
				'imageURL': banner.result_file.url, # fix it with site prefix
				'height': banner.result_file.height,
				'width': banner.result_file.width,
				'url': data['url'],
			}])
			banner.save()
			if request.is_ajax():
				return HttpResponse(json.dumps({'success': response, 'banner_id': banner.banner_id}))
			else:
				return HttpResponseRedirect(reverse('banner-detail', banner_id=banner.banner_id))


class BannerDetail(XMLRPCReadMethodMixin, TemplateView):
	method = 'getBanner'
	shared_template = 'banner_detail.jinja'

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('banner_id'))]


class BannerZones(XMLRPCReadMethodMixin, TemplateView):
	method = 'getZoneListByPublisherId'
	template_name = 'deathtrap/base/list.jinja'
	shared_template = 'banner_zones.jinja'

	def get_context_data(self, **kwargs):
		banner = Banner.objects.get(banner_id=kwargs.get('banner_id'))
		context = super(BannerZones, self).get_context_data(**kwargs)
		context["js_context"] = json.dumps({
			"banner_id": kwargs.get('banner_id'),
			"width": banner.result_file.width,
			"height": banner.result_file.height,
		})
		return context

	def get_rpc_args(self, **kwargs):
		return [1]


class BannerZoneLink(XMLRPCReadMethodMixin, TemplateView):
	method = 'generateTags'
	shared_template = 'banner_zone_link.jinja'

	def get(self, request, *args, **kwargs):
		xmlrpc_call('linkBanner', [int(kwargs.get('zone_id')), int(kwargs.get('banner_id'))])
		context = self.get_context_data(**kwargs)
		if request.is_ajax():
			return HttpResponse(json.dumps(context))
		return self.render_to_response(context)

	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('zone_id')), "invocationTags:oxInvocationTags:adjs", {}]


class BannerList(XMLRPCReadMethodMixin, TemplateView):
	method = 'getBannerListByCampaignId'
	template_name = 'deathtrap/base/list.jinja'
	shared_template = 'banner_list.jinja'

	def get_context_data(self, **kwargs):
		context = super(BannerList, self).get_context_data(**kwargs)
		context["js_context"] = json.dumps({"campaign_id": kwargs.get('campaign_id')})
		return context


	def get_rpc_args(self, **kwargs):
		return [int(kwargs.get('campaign_id'))]
