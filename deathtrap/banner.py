import io
import os
import PIL.Image, PIL.ImageDraw, PIL.ImageFont
import requests
from django.conf import settings

class Painter(object):
	def __init__(self, dimensions, layers):
		self.layers = layers
		self.image = PIL.Image.new("RGBA", dimensions, (255, 255, 255, 0))
		self._draw = PIL.ImageDraw.Draw(self.image)

	def draw(self):
		[self.draw_layer(layer) for layer in self.layers if layer]
		with io.BytesIO() as f:
			self.image.save(f, format='PNG')
			return f.getvalue()

	def draw_layer(self, layer):
		if layer['type'] == 'layer':
			raise AttributeError("Yeah, no.")
		getattr(self, 'draw_{}'.format(layer['type']))(layer)

	def draw_text(self, layer):
		font_name = "arialbold.ttf" if layer['bold'] else "arial.ttf"
		font = PIL.ImageFont.truetype(
			os.path.join(os.path.join(os.path.join(settings.BASE_DIR, "adfront"), "assets"), font_name),
			layer.get('font_size', 14))
		color = tuple(layer.get('color', (0, 0, 0))) + (255,)
		self._draw.text((layer['position']['left'], layer['position']['top']), layer['value'],
			color, font)

	def draw_image(self, layer):
		r = requests.get(layer['source'])
		source = PIL.Image.open(io.BytesIO(r.content))
		self.image.paste(source, (layer['position']['left'], layer['position']['top']),
			mask=source if source.mode == 'RGBA' else None)

	def draw_background(self, layer):
		pass