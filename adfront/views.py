from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from django.views.generic import FormView, DetailView, ListView
from crispy_forms.utils import render_crispy_form
from adfront.forms import UserForm

class UserAdd(FormView):
	form_class = UserForm
	template_name = 'registration/user_add.jinja'
	
	def get_context_data(self, **kwargs):
		context = super(UserAdd, self).get_context_data(**kwargs)
		context['form'] = render_crispy_form(context['form'], context=RequestContext(self.request))
		return context

	def get_success_url(self):
		return reverse_lazy('user-detail', kwargs={'user_id':self.user_id})

	def form_valid(self, form):
		user = form.save()
		self.user_id = user.pk
		messages.success(self.request, "A new user has been successfully added")
		return super(UserAdd, self).form_valid(form)


class UserDetail(DetailView):
	model = get_user_model()
	template_name = 'registration/user_detail.jinja'
	pk_url_kwarg = 'user_id'
	context_object_name = 'requested_user'


class UserList(ListView):
	model = get_user_model()
	template_name = 'registration/user_list.jinja'
	context_object_name = 'users'