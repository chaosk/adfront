// using jQuery
function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
	// these HTTP methods do not require CSRF protection
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
	beforeSend: function(xhr, settings) {
		if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
			xhr.setRequestHeader("X-CSRFToken", csrftoken);
		}
	}
});

function poll_task(options) {
	var interval_id,
		el = this,
		url = '/task/status/' + options.task_id + '/';
	if (typeof(options.on_success) !== 'function') {
		options.on_success = function(){};
	}
	if (typeof(options.on_failure) !== 'function') {
		options.on_failure = function(){};
	}
	if (typeof(options.on_error) !== 'function') {
		options.on_error = function(){};
	}
	if (typeof(options.check_interval) === 'undefined') {
		options.check_interval = 500;
	}
	if (typeof(options.retries) === 'undefined') {
		options.retries = 10;
	}
	function response_handler(data) {
		options.retries--;
		if (data === null)
			return;
		var task = data.task;
		if (task === null)
			return;
		if (task.status == 'PENDING') 
			return;
		if (task.status == 'SUCCESS') {
			clearInterval(interval_id);
			options.on_success(task, el);
		}
		if (task.status == 'FAILURE') {
			clearInterval(interval_id);
			options.on_failure(task, el);
		}
	}
	function poll() {
		if (options.retries <= 0) {
			options.on_error();
			clearInterval(interval_id);
		}
		$.ajax({
			url: url,
			success: response_handler,
			error: function() { options.retries--; },
			cache: false,
			dataType: 'json'
		});
	}
	$(document).ready(function() {
		setTimeout(poll, 0);
		interval_id = setInterval(poll, options.check_interval);
	});
};

function task_get(options) {
	poll_task({
		task_id: options.task_id,
		on_success: function(task, el) {
			context = options.context ? options.context : {};
			context['task'] = task;
			$('#result').append(nunjucks.render(options.template_name, context));
		},
		on_failure: function(task, el) {
			$('#state').html(task.status);
			$('#result').append("<pre>" + $('<div />').text(task.traceback).html() + "</pre>");
		},
		on_error: function() {
			$('#state').html("ERROR max number of retries reached");
		}
	});
}

function task_post(options) {
	options.form.on("submit", function(e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			data: options.form.serialize(),
			dataType: 'json',
			success: function(data) {
				if (!(data.success)) {
					options.form.replaceWith(data.form_html);
				} else {
					poll_task({
						task_id: data.task_id,
						on_success: function(task, el) {
							window.location.href = options.success_url.replace(999, task.result);
						},
						on_failure: function(task, el) {
							$('#state').html(task.status);
							$('#result').append("<pre>" + $('<div />').text(task.traceback).html() + "</pre>");
						}
					});
				}
			},
		});
	});
}
