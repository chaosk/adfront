from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from adfront.views import UserAdd, UserDetail, UserList

urlpatterns = [
	url(r'^', include('deathtrap.urls')),
	url(r'^task/status/(?P<task_id>.+)/$', 'djcelery.views.task_status',
		name='task-status'),
	url(r'^$', TemplateView.as_view(template_name="home.jinja"), name="home"),
	url(r'', include('django.contrib.auth.urls')),
	url(r'^user/add/$', UserAdd.as_view(), name="user-add"),
	url(r'^user/detail/(?P<user_id>\d+)/$', UserDetail.as_view(), name="user-detail"),
	url(r'^user/list/$', UserList.as_view(), name="user-list"),
]

if settings.DEBUG:
	from django.conf.urls.static import static
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)