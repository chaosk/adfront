from django.core.urlresolvers import reverse
from django.contrib.auth import models as auth_models
from django.db import models

class User(auth_models.AbstractUser):
	revive_advertiser_id = models.IntegerField(blank=True, null=True)

	def get_absolute_url(self):
		return reverse('user-detail', kwargs={'user_id':self.id})